#!/usr/bin/env bash

# Dependencies: zenity, poppler(-utils), libreoffice-draw, xdg-utils

tmp_pipe="/tmp/converts_print_zenity000_$(date +%s)"
rm -fvr "${tmp_pipe:?}/*"
start_time="$(env LANG=c date).log"

zenity_file_selection(){
	zenity \
		--file-selection \
		--width=750 --height=560 \
		--window-icon=/usr/share/icons/converts_print.svg \
		"$@"
}

#zenity_window(){
#	zenity \
#	 --window-icon=/usr/share/icons/converts_print.svg \
#	 $@
#}

# неплохо было бы вынести обработку создания директорий в функцию, чтобы не дублировать код

data_dir="$HOME/Печать конвертов"
if [ -d "$data_dir" ]
	then
		echo "Директория  сданными программы существует, не будем ее создавать."
	else
		echo "Директория работы программы не существует, создадим ее..."
		mkdir "$data_dir"
		if [ -d "$data_dir" ]
			then
				echo "Проверка пройдена, директория с данными программы создалась"
			else
				echo "ОШИБКА!!! Не получилось создать директорию $data_dir !!! Завершаем работу!"
				exit 1
		fi
fi

db_dir="$data_dir/Базы конвертов"
if [ -d "$db_dir" ]
	then
		echo "Директория с базами конвертов существует, не будем ее создавать."
	else
		echo "Директория с базами конвертов не существует, создадим ее..."
		mkdir "$db_dir"
		if [ -d "$db_dir" ]
			then
				echo "Проверка пройдена, директория с базами конвертов создалась"
			else
				echo "ОШИБКА!!! Не получилось создать директорию $db_dir !!! Завершаем работу!"
				exit 1
		fi
fi

convert_templates_dir="$data_dir/Шаблоны конвертов"
if [ -d "$convert_templates_dir" ]
	then
		echo "Директория с шаблонами конвертов существует, не будем ее создавать."
	else
		echo "Директория с шаблонами конвертов не существует, создадим ее..."
		mkdir "$convert_templates_dir"
		if [ -d "$convert_templates_dir" ]
			then
				echo "Проверка пройдена, директория с шаблонами конвертов создалась"
			else
				echo "ОШИБКА!!! Не получилось создать директорию $convert_templates_dir !!! Завершаем работу!"
				exit 1
		fi
fi

converts_dir="$data_dir/Готовые конверты"
if [ -d "$converts_dir" ]
	then
		echo "Директория с готовыми конвертами существует, не будем ее создавать."
	else
		echo "Директория с готовыми конвертами не существует, создадим ее..."
		mkdir "$converts_dir"
		if [ -d "$converts_dir" ]
			then
				echo "Проверка пройдена, директория с данными программы создалась"
			else
				echo "ОШИБКА!!! Не получилось создать директорию $converts_dir !!! Завершаем работу!"
				exit 1
		fi
fi

global_data_dir="/usr/share/converts_print"
if [ -d "$global_data_dir" ]; then
	# copy without overwriting
	cp -rvn ${global_data_dir}/* "${data_dir}/"
fi

logs_dir="$data_dir/Логи"
if [ -d "$logs_dir" ]
	then
		echo "Директория с логами существует, не будем ее создавать."
	else
		echo "Директория с логами не существует, создадим ее..."
		mkdir "$logs_dir"
		if [ -d "$logs_dir" ]
			then
				echo "Проверка пройдена, директория с логами создалась"
			else
				echo "ОШИБКА!!! Не получилось создать директорию $converts_dir !!! Завершаем работу!"
				exit 1
		fi
fi

pdf_all_dir="$data_dir/PDF многостарничные для распечатки"
if [ -d "$pdf_all_dir" ]
	then
		echo "Директория с многостр PDF существует, не будем ее создавать."
	else
		echo "Директория с многостр PDF не существует, создадим ее..."
		mkdir "$pdf_all_dir"
		if [ -d "$pdf_all_dir" ]
			then
				echo "Проверка пройдена, директория с данными программы создалась"
			else
				echo "ОШИБКА!!! Не получилось создать директорию $converts_dir !!! Завершаем работу!"
				exit 1
		fi
fi

converts_archive_dir="$data_dir/Архив конвертов"
if [ -d "$converts_archive_dir" ]
	then
		echo "Директория с архивом конвертов существует, не будем ее создавать."
	else
		echo "Директория с архивом конвертов не существует, создадим ее..."
		mkdir "$converts_archive_dir"
		if [ -d "$converts_archive_dir" ]
			then
				echo "Проверка пройдена, директория с данными программы создалась"
			else
				echo "ОШИБКА!!! Не получилось создать директорию $converts_dir !!! Завершаем работу!"
				exit 1
		fi
fi

function zenity_choose_workway {
	zenity --list --width=1270 --height=500 --title="Печать конвертов" \
       --text="Выберите режим работы									" \
       --column="Режим работы" \
       "Выбор адреса и предприятия из базы" \
       "Ввод адреса, индекса, названия и типа арбитр.упр. вручную" \
       "Сделать конверты для всех записей в базе" \
       "Выйти из программы"
}

function validate_converts_template {
	# эта функция должна вызываться изнутри функции process_doc!!
	echo "Выполним проверку валидности шаблона конвертов"
	
	for validate_pattern in 888888 ТИП_УПРАВЛЯЮЩЕГО ПРЕДПРИЯТИЕ_НАЗВАНИЕ АДРЕС_НАЗНАЧЕНИЯ АДРЕС_НАЗНАЧЕНИЯ DB_ID
	do
		# The exit status is 0 (true) if the name was found, 1 (false) if not
		# https://stackoverflow.com/questions/4749330/how-to-test-if-string-exists-in-file-with-bash-shell
		if grep -Fq "$validate_pattern" content.xml
			then
				# code if found
				echo "Строка $validate_pattern найдена в шаблоне"
			else
				# code if not found
				echo "Строка $validate_pattern не найдена в шаблоне"
				zenity --error --text="Строка $validate_pattern не найдена в шаблоне, попробуйте пересохранить шаблон в Libreoffice! Не можем продолжить работу!"
				exit 1
		fi
	done
}

function process_doc {
	cd "$data_dir"
	rm -rfv tmp0
	mkdir tmp0
	unzip -d tmp0 "$convert_template_file"
	cd tmp0
	validate_converts_template
	# кавычки заменяем через sed, т.к. Canon - уебки
	cat content.xml | tr '!' '/' | sed "s!888888!$destination_index!g" | sed "s!ТИП_УПРАВЛЯЮЩЕГО!$arbitr_type!g" | sed "s!ПРЕДПРИЯТИЕ_НАЗВАНИЕ!$destination_name!g" | sed "s!АДРЕС_НАЗНАЧЕНИЯ!$destination_adress!g" | sed "s!ИМЯ_ПОЛУЧАТЕЛЯ!$destination_human!g" | sed "s!DB_ID!$add_date_id!g" | sed 's!«!\"!g' | sed 's!»!\"!g' >content2.xml
	rm -fv content.xml
	mv content2.xml content.xml
	#name_date="`date +%s`"
	name_date="$add_date_id"
	zip -0Xq "$name_date".odg mimetype
	zip -Xr9Dq "$name_date".odg *
	cd ..
	mv tmp0/"$name_date".odg "$converts_dir/"$name_date".odg"
	rm -rfv tmp0
	cd "$converts_dir"
	libreoffice --convert-to pdf "$converts_dir/"$name_date".odg"
	echo "$name_date" >>"$logs_dir/$start_time"
}

previous_dir="`pwd`"
cd "$db_dir"
db_file=`zenity_file_selection --title="Выбор базы конвертов"`
cd "$previous_dir"
# извращение ниже и выше с переходом в нужную директорию и обратно нужно, чтобы Zenity/YAD открывал диалог выбор шаблона конверта в нужной папке
previous_dir="`pwd`"
cd "$convert_templates_dir"
convert_template_file=`zenity_file_selection --title="Выберите шаблон конверта .odg"`
echo "Выбран шаблон конверта: $convert_template_file "
cd "$previous_dir"
# на всякий случай обнулим переменную, чтобы потом ее ошибочно не использовать
previous_dir="0"

while true
do
	rm -fv $tmp_pipe
	workway="$(zenity_choose_workway)"
	echo "Выбран режим работы: $workway"

	case $workway in
		"Ввод адреса, индекса, названия и типа арбитр. упр. вручную")
			
			if [ -z "$saveornot" ]; then
				saveornot="$(zenity --list --title="Сохранять ли в БД?" --text="Нужно ли сохранять вручную вводимые данные (индекс, адрес, название предприятия) в базу данных?" --column="Вариант" "ДА" "НЕТ")"
			fi
			
			destination_adress="$(zenity --entry --title="Ввод адреса" --text="Введите полностью АДРЕС назначения письма без индекса:")"
			echo "Адрес назначения: $destination_adress"
			destination_human="$(zenity --entry --title="Ввод имени человека-получателя" --text="Введие ФИО человека-получателя в именительном падеже:")"
			echo "ФИО получателя: $destination_human"
			destination_index="$(zenity --entry --title="Ввод индекса" --text="Введите ИНДЕКС назначения (6 цифр):")"
			echo "Индекс назначения: $destination_index"
			destination_name="$(zenity --entry --title="Ввод названия предприятия" --text="Введите НАЗВАНИЕ ПРЕДПРИЯТИЯ-получателя письма:")"
			echo "Название предприятия назначения: $destination_name"
			arbitr_type="$(zenity --list --title="Выберите тип арбитражного управления" --text="Выберите ТИП арбитражного УПРАВЛЕНИЯ" --column="Вариант" "конкурсный" "финансовый" "временный")"
			echo "Тип арбитражного управления: $arbitr_type"
			
			if  [ "$saveornot" = "ДА" ]
				then
					date_add_unix="`date +%s`"
					date_add_human="`date`"
					
					# сделаем нулевыми значения пустых переменных, чтобы не было бардака в БД
					#if [ -z "$destination_name" ]
						#then
							#destination_name="0"
					#fi
					#if [ -z "$destination_adress" ]
						#then
							#destination_adress="0"
					#fi
					#if [ -z "$destination_index" ]
						#then
							#destination_index="0"
					#fi
					#if [ -z "$arbitr_type" ]
						#then
							#arbitr_type="0"
					#fi
					
					echo "$destination_name;$destination_adress;$destination_index;$arbitr_type;$destination_human;$date_add_unix;$date_add_human" >>"$db_file"
			fi
		;;
			
		"Выбор адреса и предприятия из базы")
			
			num="0"
			while read line
			do
			num="$[ num + 1 ]"
			destination_name=`echo $line | awk -F ";" '{print $1}'`
			destination_adress=`echo $line | awk -F ";" '{print $2}'`
			destination_index=`echo $line | awk -F ";" '{print $3}'`
			arbitr_type=`echo $line | awk -F ";" '{print $4}'`
			destination_human=`echo $line | awk -F ";" '{print $6}'`
			add_date_id=`echo $line | awk -F ";" '{print $5}'`
			echo "$add_date_id" >>$tmp_pipe
			echo "$destination_name" >>$tmp_pipe
			echo "$destination_adress" >>$tmp_pipe
			echo "$destination_index" >>$tmp_pipe
			echo "$arbitr_type" >>$tmp_pipe
			echo "$destination_human" >>$tmp_pipe
			
			#if [ -z "$zenity_args" ]
				#then
					#zenity_args="$destination_name \n $destination_adress \n $destination_index \n $arbitr_type"
				#else
					#zenity_args="$zenity_args \n $destination_name \n $destination_adress \n $destination_index \n $arbitr_type"
			#fi
			## zenity_args="$zenity_args $destination_name $destination_adress $destination_index $arbitr_type"
			
			done < "$db_file"
			
			selected_db_item="$(cat $tmp_pipe | zenity --list --width=1270 --height=950 --title="Выбор из базы данных; по нажатию на название колонки работает сортировка" --column="Дата добавления в базу" --column="Название предприятия" --column="Адрес" --column="Индекс" --column="Тип арбитр.упр." --column="ФИО получателя/предприятия")"
			echo "$selected_db_item"
			line=`cat "$db_file" | grep "$selected_db_item" | head -n 1`
			destination_name=`echo $line | awk -F ";" '{print $1}'`
			destination_adress=`echo $line | awk -F ";" '{print $2}'`
			destination_index=`echo $line | awk -F ";" '{print $3}'`
			arbitr_type=`echo $line | awk -F ";" '{print $4}'`
			destination_human=`echo $line | awk -F ";" '{print $6}'`
			add_date_id=`echo $line | awk -F ";" '{print $5}'`
			process_doc
		;;
		
		"Сделать конверты для всех записей в базе")
			echo "Перемещаем существующие готовые конверты в архив"
			#mv "$converts_dir/*" "$converts_archive_dir/"
			for i in `ls "$converts_dir"`; do mv "$converts_dir/$i" "$converts_archive_dir/$i"; done
			while read line
			do
				echo " "
				echo LINE
				echo "$line"
				echo " "
				destination_name=`echo $line | awk -F ";" '{print $1}'`
				destination_adress=`echo $line | awk -F ";" '{print $2}'`
				destination_index=`echo $line | awk -F ";" '{print $3}'`
				arbitr_type=`echo $line | awk -F ";" '{print $4}'`
				destination_human=`echo $line | awk -F ";" '{print $6}'`
				add_date_id=`echo $line | awk -F ";" '{print $5}'`
				process_doc
			done < "$db_file"
			# https://stackoverflow.com/a/11280219
			pdfunite *.pdf ALL.pdf
			date_unix="`date +%s`"
			mv ALL.pdf "$pdf_all_dir/ALL_$date_unix.pdf"
			xdg-open "$pdf_all_dir/ALL_$date_unix.pdf"
		;;
		
		"Выйти из программы")
			exit
		;;
		
	esac

done

