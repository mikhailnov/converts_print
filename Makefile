#
#PREFIX = $(HOME)
PREFIX = /usr

all:
	@ echo "Nothing to compile. Use: make install"

install:
	install -d $(DESTDIR)/$(PREFIX)/bin
	install -d $(DESTDIR)/$(PREFIX)/share/converts_print
	install -d $(DESTDIR)/$(PREFIX)/share/icons
	install -d $(DESTDIR)/$(PREFIX)/share/applications
	install -m0755 ./converts_print.sh $(DESTDIR)/$(PREFIX)/bin/converts_print
	install -m0644 ./desktop/converts_print.desktop $(DESTDIR)/$(PREFIX)/share/applications/
	install -m0644 ./desktop/converts_print.svg $(DESTDIR)/$(PREFIX)/share/icons/
	cp -rv "Шаблоны конвертов" $(DESTDIR)/$(PREFIX)/share/converts_print/
	cp -rv "Шаблон базы" $(DESTDIR)/$(PREFIX)/share/converts_print/
	#chmod -R 0644 $(DESTDIR)/$(PREFIX)/share/converts_print/
 
